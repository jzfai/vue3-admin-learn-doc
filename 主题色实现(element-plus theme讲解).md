# 前言

在element-plus中主题色的设置主要分为两个部分，1.基础色， 2. 各个组件主题色更改



##### 主题色修改原理：利用css3中的root和var进行主题色控制，root定义全局变量，var引入。通过动态修改root中的变量实现主题色修改 



## 基础色

element-plus中基础色分为 white  black primary, success, warning, danger, error, info 由一个 **$colors ** 控制

```scss
//src/theme/base/element-plus/var.scss
// change color
$colors: () !default;
$colors: map.deep-merge(
  (
    'white': #ffffff,
    'black': #000000,
    'primary': (
      'base': #c72210//#409eff
    ),
    'success': (
      'base': #45b207
    ),
    'warning': (
      'base': #ec8828
    ),
    'danger': (
      'base': #f56c6c
    ),
    'error': (
      'base': #d24934
    ),
    'info': (
      'base': #909399
    )
  ),
  $colors
);
```

我们通过修改基础色中的颜色，已达到修改基础色

当你修改完基础色后，相应的基础变量

```scss
--el-color-primary: #c72210;
--el-color-success: #45b207;
```



## 组件主题色更改

我们以el-button为例，当我们运行页面后查看el-button的变量

![1670484619673](assets/1670484619673.png)

那么以上就是我们要修改的变量，如：

```scss
//src/theme/base/element-plus/button.scss
--el-button-text-color: #262626;
--el-button-bg-color: #ffffff;
--el-button-border-color: #d9d9d9;
--el-button-outline-color: #d9d9d9;

--el-button-hover-text-color: #c72210;
--el-button-hover-link-text-color: #c72210;
--el-button-hover-bg-color: #ffece6;
--el-button-hover-border-color: transparent;

--el-button-active-color: #a8150a;
--el-button-active-bg-color: #a8150a;
--el-button-active-border-color: transparent;

--el-button-disabled-text-color: #a6a6a6;
--el-button-disabled-bg-color: #ffece6;
--el-button-disabled-border-color: #c72210;
//loading
--el-button-loading-text-color: #c72210;
--el-button-loading-bg-color: #ffece6;
--el-button-loading-border-color: #c72210;
```

那么此时修改的变量会覆盖以前的变量，变量修改遵循 “最近原则”



当然你怕全局变量污染，你也可以给变量加一层class  如：

```scss
.at-button-low {
    --el-button-text-color: #262626;
    --el-button-bg-color: #ffffff;
    --el-button-border-color: #d9d9d9;
    --el-button-outline-color: #d9d9d9;
    //....
  }
```

当你使用  at-button-low  className时修改的变量生效



总的来说：我们通过动态修改css3的变量来实现我们主题色的修改

