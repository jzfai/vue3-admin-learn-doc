## 前言

进行全局错误收集，有助于我们及时收集到报错信息。特别是在生产环境中，能及时收集到用户使用时的错误日志。第一时间处理，提高系统的可靠性，和用户体验性，为公司减少不必要的损失。全局错误收集是多么重要。

那么本篇就来主要讲讲如何更优雅的收集错误。

##### 错误日志分为：

JS运行时错误

>正常js运行时，Throw New Error("JS运行时错误")等的错误

资源加载错误

>img，script， link  中加载资源错误  

promise被reject未被处理产生错误:

>new Promise  reject后，未catch产生的错误

console.error错误:

>console.error(错误)生成的错误

请求错误(跨域错误，401，404，500)

>发送ajax请求，或者fatch的错误收集，如跨域，401,404等错误

那么如何收集呢，请看下面详细介绍



### 安装依赖

```shell
pnpm add "js-error-collection@1.0.7" -S
```



#### 配置

新建 src/hooks/useErrorLog.js

```javascript
import { jsErrorCollection } from 'js-error-collection'
const errorLogReq = (errLog) => {
  //自身errorLog不发送
  if (errLog.includes('errorCollection/insert')) return
  //发送错误日志到后台
  // request({
  //   url: '/integration-front/errorCollection/insert',
  //   data: {
  //     pageUrl: window.location.href,
  //     errorLog: errLog,
  //     browserType: navigator.userAgent,
  //     version: pack.version
  //   },
  //   method: 'post',
  //   bfLoading: false,
  //   isAlertErrorMsg: true
  // }).then(() => {
       //通知错误日志页面更新数据
  //   bus.emit('reloadErrorPage', {})
  // })
}

export default function () {
  //收集错误日志，并传递给errorLogReq
  jsErrorCollection({ runtimeError: true, rejectError: true, consoleError: true }, (errLog) => {
      console.log(errLog)
      errorLogReq(errLog)
    })
}
```



main.js中引入useErrorLog

```javascript
//error log  collection
import errorLog from '@/hooks/useErrorLog'
errorLog(app)
```



#### 使用

新建测试页面 src/views/error-log/ErrorLogTest.vue

```vue
<template>
  <div class="scroll-y">
     <!--promise被reject未被处理产生错误:-->
    <div class="mt-2">throw unhandledrejection</div>
     <!--console.error错误:-->
    <el-button type="primary" @click="handle">unhandledrejection</el-button>
    <div class="mt-2">throw console.error</div>
    <el-button type="primary" @click="consoleErrorFun">console.error</el-button>
    <div class="mt-2">throw normalError</div>
     <!--请求错误(跨域错误，401，404，500)-->
    <el-button type="primary" @click="normalError">normalError</el-button>
    <div class="mt-2">throw req cross origin</div>
    <el-button type="primary" @click="reqCrossOrigin">reqCrossOrigin</el-button>
    <div class="mt-2">throw req 404 error</div>
    <el-button type="primary" @click="req404">req404</el-button>
    <div class="mt-2">throw image load error</div>
    <!--资源加载错误-->
    <el-button type="primary" @click="errorLogImg">imageLoadError</el-button>
    <img v-if="imgShow" src="http://img.png" />
  </div>
</template>
<script setup>
const appStore = useAppStore()
let settings = computed(() => {
  return appStore.settings || {}
})
const handle = () => {
  new Promise((resolve, reject) => {
    reject('reject promise')
  }).then((res) => {
    console.log('ok')
  })
}
let flag = ref(null)
const consoleErrorFun = () => {
  console.error('console.error')
}
const normalError = () => {
  throw new Error(' throw new Error("")\n')
}
let reqCrossOrigin = () => {
  axiosReq({
    baseURL: 'https://github.jzfai.top/micro-service-test',
    url: '/integration-front/brand/updateBy',
    data: { id: 'fai' },
    method: 'put',
    isParams: true,
    bfLoading: true
  }).then(() => {})
}

import axiosReq from '@/utils/axiosReq'
import { useAppStore } from '@/store/app'
let req404 = () => {
  axiosReq({
    // baseURL: 'https://github.jzfai.top/micro-service-test',
    url: '/integration-front/brand/updateBy1',
    data: { id: 'fai' },
    method: 'put',
    isParams: true,
    bfLoading: true
  }).then((res) => {})
  //the error will collection to unhandledrejection if you  no catch
  // .catch((err) => {})
}

//img loader err test
let imgShow = ref(false)
const errorLogImg = () => {
  imgShow.value = !imgShow.value
}
</script>
```

>通过测试页面进行测试，发现当有错误日志时，useErrorLog不断打印错误日志