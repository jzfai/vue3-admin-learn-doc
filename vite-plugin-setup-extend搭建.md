# 前言

本篇介绍实现两个功能：1.index.html 自动注入title; 2. 扩展 script-setup 设置组件名name 

```typescript
import { parse } from '@vue/compiler-sfc'
import { render } from 'ejs'
import type { Plugin } from 'vite'
export default ({ inject }): Plugin => {
  // let viteConfig
  return {
    name: 'vite-plugin-setup-extend',
    enforce: 'pre',
    // configResolved(resolvedConfig) {
    //   viteConfig = resolvedConfig
    // },
    //index.html 自动注入title
    async transformIndexHtml(html) {
      const result = await render(html, { ...inject })
      return result
    },
    //扩展 script-setup 设置组件名name 
    transform(code, id) {
      if (/\.vue$/.test(id)) {
        const { descriptor } = parse(code)
        if (!descriptor?.scriptSetup?.setup) {
          return null
        }
        const { lang, name } = descriptor.scriptSetup?.attrs || {}
        const dillStr = headString(lang, name)
        code += dillStr
        return code
      }
    }
  }
}
//设置组件名name，需要插入的脚本 
const headString = (lang, name) => {
  return `<script ${lang ? `lang="${lang}"` : ''}>
import { defineComponent } from 'vue'
export default defineComponent({
  ${name ? `name: "${name}",` : ''}
})
</script>\n`
}
```

vite.config.js中引入

```javascript
import setupExtend from './src/plugins/vite-plugin-setup-extend'
plugins: [
    setupExtend({ title: '熊猫哥' })
]
```







