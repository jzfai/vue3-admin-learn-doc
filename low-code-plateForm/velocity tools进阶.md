# 一. VelocityTools介绍

[velocity tools官方文档](https://velocity.apache.org/tools/devel/tools-summary.html#CollectionTool)



## 1.1 VelocityTools简介

Velocity Tools 是 Velocity模板引擎的一个子项目，用于将 Velocity 与 Web开发环境集成的工具包。

## 1.2 VelocityTools的组成

VelocityTools项目分为两个部分：`GenericTools`和`VelocityView` . 

- GenericTools  : `GenericTools`是一组类，它们提供在标准Velocity项目中使用工具的基本基础结构，以及在通用Velocity模板中使用的一组工具。例如 : DateTool、NumberTool和RenderTool很多其他可用的工具
- Velocity view : 包括所有的通用工具结构和在web应用程序的视图层中使用Velocity的专用工具。这包括用于处理Velocity模板请求的`VelocityViewServlet`或`VelocityLayoutServlet`、用于在JSP中嵌入Velocity的`VelocityViewTag`和用于在Velocity模板中嵌入JSP标记库的Maven插件。这里流行的工具是LinkTool和ParameterTool。

# 二. GenericTools使用

## 2.1 GenericTools介绍

GenericTools  : `GenericTools`是一组类，它们提供在标准Velocity项目中使用工具的基本基础结构，以及在通用Velocity模板中使用的一组工具。

简单来说, GenericTools就是`Velocity`官方提供的一组可以在模板中使用的工具类库



### 引入坐标

```velocity
<!--velocity模板工具-->
<dependency>
  <groupId>org.apache.velocity.tools</groupId>
  <artifactId>velocity-tools-generic</artifactId>
  <version>3.1</version>
</dependency>
```



## 2.2.4 编写配置

velocity-tools.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<tools>
   <!--scope : 指定工具类的作用范围, 取值有三种 : application , request , session -->
    <toolbox scope="application">
        <tool key="number" class="org.apache.velocity.tools.generic.NumberTool"/>
        <tool class="org.apache.velocity.tools.generic.DateTool" format="yyyy-MM-dd"/>
    </toolbox>
</tools>
```

使用

```velocity
当前时间 : $date.get('yyyy-MM-dd HH:mm:ss')
```





## 2.3 工具类及案例

格式化工具类的主要作用就是对数据进行格式化之后输出 , 例如 : 日期格式化 , 数字格式化等 , GenericTools提供的工具类有很多 , 随着时间的推移很多工具类已经过期, 有更好更安全的替代方案, 这里我们仅仅介绍一些常用工具类

### **2.3.1 DateTool**

**DateTool**用于访问和格式化日期以及格式化Date和Calendar对象。该工具还可以用于在各种日期类型之间进行转换。

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
年 : $date.getYear()
月: $date.getMonth()
日: $date.getDay()

当前时间 : $date.format($now)
当前时间 : $date.format("yyyy-MM-dd HH:mm:ss",$now)
当前时间 : $date.get('yyyy-MM-dd HH:mm:ss')
</body>
</html>
```

**配置**

```xml
<toolbox scope="application">
    <tool  key="date" class="org.apache.velocity.tools.generic.DateTool" format="yyyy-MM-dd"></tool>
</toolbox>
```

> 格式基本上是固定的 , <toolbox>代表配置一个工具盒 , 里面可以配置很多个工具`<tool>` , 这些表情上的属性有很多
>
> - scope : 此工具的作用范围 , `request`，`session`或`application`。
> - key :  此工具映射到的上下文键。列入当前key是date , 那么在模板中既要使用`$date`来使用工具 , 从VelocityTools 2开始，工具箱可以根据其类名自动确定工具的key。一个名为org.com.FooTool的工具将在模板中分配键$foo，名为org.com.FooBarTool的工具为$ fooBar，一个名为org.com.FooBar的工具也为$ fooBar。
> - class : 指定工具类的完全限定路径
> - format : 在使用DateTool工具的时候, 指定时间日期格式化得格式



### **2.3.2 NumberTool**

**NumberTool**用于访问和格式化任意数值类型对象。该工具还可以用于检索`NumberFormat`实例或与各种数字类型进行相互转换。

模板

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
原始数据 : $myNumber
格式化 : $number.format($myNumber)
取整 : $number.integer($myNumber)
</body>
</html>
```

配置

```xml
<toolbox scope="application">
    <tool key="number" class="org.apache.velocity.tools.generic.NumberTool" />
</toolbox>
```

### 2.3.3 MathTool

**MathTool**用于在Velocity中执行数学运算。

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

num1+num2 : $math.add($num1,$num2);
num1-num2 : $math.sub($num1,$num2);
num1*num2 : $math.mul($num1,$num2);
num1/num2 : $math.div($num1,$num2);
向上取整 : $math.ceil($math.div($num1,$num2))
向下取整 : $math.floor($math.div($num1,$num2))
四舍五入 : $math.roundTo(2,$math.div($num1,$num2))  ## 第一个参数保留的位数 , 第二个参数运算的值


</body>
</html>
```

**配置**

```xml
<toolbox scope="application">
    <tool key="math" class="org.apache.velocity.tools.generic.MathTool" />
</toolbox>
```



### **2.3.4 DisplayTool**

用于控制数据显示和隐藏 , 以及数据格式的处理

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

## list方法用于展示数据或集合中的数据 , 默认的展示格式为 A, B and C
默认输出格式 : $display.list($list)
使用,分割输出 : $display.list($list,",")

## truncate方法用于字符串截取 , 默认截取30个长度
字符串截取, 默认30个长度 : $display.truncate("truncate方法用于字符串截取默认截取30个长度")
字符串截取, 给定20个长度 : $display.truncate("truncate方法用于字符串截取默认截取30个长度",20)
字符串截取, 给定20个长度 : $display.truncate("truncate方法用于字符串截取默认截取30个长度",20,"")

## alt方法用于判断给定的数据是否为空 , 如果为空展示第二个参数 , 如果不为空展示数据本身
不为空：$display.alt($num1,"num1不为空")
为空：$display.alt($num3,"num3为空")

</body>
</html>
```

**配置**

```
<toolbox scope="application">
    <tool key="display" class="org.apache.velocity.tools.generic.DisplayTool"/>
</toolbox>
```



### 2.3.5 EscapeTool

用于对一些特殊字符进转义处理 , 例如 `$` , `# `, `&` 等...

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

    $velocity
    $esc.velocity($velocity)

    $html
    $esc.html($html)

    $url
    $esc.url($url)
    $esc.unurl($esc.url($url))

    $esc.dollar     ## $
    $esc.d          ## $

    $esc.hash       ## #
    $esc.h          ## #

    $esc.backslash  ## \
    $esc.b          ## \

    $esc.quote      ## "
    $esc.q          ## "

    $esc.singleQuote    ## '
    $esc.s              ## '

    $esc.exclamation    ## !
    $esc.e              ## !

</body>
</html>
```

**配置**

```xml
<toolbox scope="application">
    <tool key="esc" class="org.apache.velocity.tools.generic.EscapeTool"/>
</toolbox>
```



### 2.3.6 FieldTool

用于访问类中定义的静态常量

**常量类**

定义`MyConstants`常量类

```
package com.jzfai.constants;

public class MyConstants {
    public static  String COUNTER_NAME = "COUNTER";
}
```

定义`Counter`常量类

```
public class Counter {
   public static Integer MAX_VALUE = 100 ;
   public static Integer MIN_VALUE = 100 ;
}
```

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

## 访问在配置中定义的静态常量
获取MyConstants中的常量 : $field.COUNTER_NAME

## 通过一个类中的常量
获取Counter类中的量 : $field.in("com.jzfai.counter.Counter").MAX_VALUE

## 传入一个对象的实例 , 通过对象的实例获取其类中定义的常量
获取日历对象中的YEAR常量 : $field.in($calender).YEAR

## 默认情况下, 当我们查找了一个类的常量之后, 这个类回保存在FieldTool工具中, 可以直接获取下一个常量
获取日历对象中的DATE常量 : $field.DATE  ## 因为之前已经获取过 , 所以可以直接获取

</body>
</html>
```

**配置**

```xml
<toolbox scope="application">
    <tool key="field"  class="org.apache.velocity.tools.generic.FieldTool" include="com.jzfai.constants.MyConstants"/>
</toolbox>
```

> include属性可以引入一些类, 引入之后想要获取其中的常量, 直接使用 `$field.常量字段名称`即可 !  引入多个类以`,`分割

### 2.3.7 ClassTool

ClassTool用于访问一个类的`Class`对象信息以及其`Filed` , `Method` , `Constructor`等信息 , 它的设计没有考虑到代码的反射执行，因此无法通过反射执行代码。



**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

## 获取要查看的类上的所以注解 , 只有运行时期的注解才能够获取到
注解 : $class.getAnnotations()
构造方法 :
#foreach($constructor in $class.getConstructors())
    $constructor
#end
属性 :
#foreach($f in $class.getFields())
    $f
#end
方法 :
#foreach($m in $class.getMethods())
    $m
#end
包名 : $class.getPackage()
类名 : $class.getName()

## 也可以不通过配置文件 , 自己指定一个要查找的类
包名 : $class.inspect("java.lang.String").getPackage()
类名 : $class.inspect("java.lang.String").getName()
构造方法 :
#foreach($constructor in $class.inspect("java.lang.String").getConstructors())
    $constructor
#end
</body>
</html>
```

**配置**

```xml
<toolbox scope="application">
    <tool class="org.apache.velocity.tools.generic.ClassTool" inspect="com.jzfai.utils.Result" ></tool>
</toolbox>
```

> inspect : 指定一个需要查找的类 

### 2.3.8 ContextTool

用于获取Context中保存的数据和元数据

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

context中的所有key :
#foreach( $key in $context.keys )
    $key
#end
<br>

context中的所有value :
#foreach( $value in $context.values )
    $value
#end

<br>

context中的所有key-value :
#foreach( $key in $context.keys )
    $key = $context.get($key)
#end

</body>
</html>
```

**配置**

```xml
<toolbox scope="request">
    <tool key="context" class="org.apache.velocity.tools.generic.ContextTool"/>
</toolbox>
```

> 需要注意的是在`application`作用范围中没有`ContextTool` , 所以scope需要指定为`request`



### 2.3.9 RenderTool

Render用于将给定的字符串当作VTL秩序

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
#set($list = [1,2,3] )
#set($object = '$list' )
#set($method = 'size()' )
## 将字符串当作VTL秩序
$render.eval("${object}.$method")


## 使用当前上下文递归地评估包含VTL的字符串，并将结果作为字符串返回。
#macro(say_hi)
    hello world!
#end

#set($foo = "#say_hi()")
#set($bar = "$foo" )
$render.recurse($bar)

  
</body>
</html>
```

**配置**

```xml
<toolbox scope="request">
    <tool key="render" class="org.apache.velocity.tools.generic.RenderTool"></tool>
</toolbox>
```

> 使用recurse递归时 ,RenderTool 默认将递归限制为20个周期，以防止无限循环

### 2.3.11 CollectionTool

CollectionTool允许用户对集合中包含的对象公开的任意任意属性集对集合（或数组，迭代器等）进行排序，并通过拆分字符串来生成数组。

**模板**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

简单类型排序 :
#set($strList = $collection.sort($strs))
#foreach($str in $strList)
    $str
#end

对象类型排序 - 单个字段 :
#set($users = $collection.sort($userList,"age:asc"))
#foreach($user in $users)
    $user.name : $user.age  :  $user.sex
#end

对象类型排序 - 多字段 :
#set($users = $collection.sort($userList,["sex:desc","age:asc"]))
#foreach($user in $users)
    $user.name : $user.age  :  $user.sex
#end

拆分字符串 :
#set($str="hello word , how are you !")
#foreach($s in $collection.split($str))
    $s
#end


</body>
</html>
```

**配置**

```
<tool key="collection" class="org.apache.velocity.tools.generic.CollectionTool" stringsDelimiter=" ">
</tool>
```

> stringsDelimiter : 指定进行字符串分割时的分割符 , 默认是`,`

