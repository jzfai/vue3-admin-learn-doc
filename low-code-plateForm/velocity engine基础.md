## 1. velocity简介

Velocity是一个基于Java的模板引擎，可以通过特定的语法获取在java对象的数据 , 填充到模板中，从而实现界面和java代码的分离 ! 

![image-20200604160527526](assets/image-20200604160527526.png)

## 2. 应用场景

- Web应用程序 : 作为为应用程序的视图, 展示数据。

- 源代码生成  : Velocity可用于基于模板生成Java源代码 和 前端vue源代码（重点）

- 自动电子邮件 : 网站注册 , 认证等的电子邮件模板

- 网页静态化  : 基于velocity模板 , 生成静态网页

  

## 3. velocity 组成结构

![1589436741048](assets/1589436741048.png)



Velocity主要分为app、context、runtime和一些辅助util几个部分。

- app模块 : 主要封装了一些接口 , 暴露给使用者使用。主要有两个类，分别是Velocity(单例)和VelocityEngine。

- Context模块 : 主要封装了模板渲染需要的变量

- Runtime模块 : 整个Velocity的核心模块，Runtime模块会将加载的模板解析成语法树，Velocity调用mergeTemplate方法时会渲染整棵树，并输出最终的渲染结果。

- RuntimeInstance类为整个Velocity渲染提供了一个单例模式，拿到了这个实例就可以完成渲染过程了。



## 二. 快速入门



###  引入坐标

```xml
 <!--zip压缩-->
<dependency>
  <groupId>net.lingala.zip4j</groupId>
  <artifactId>zip4j</artifactId>
  <version>2.9.1</version>
</dependency>
<!--velocity模板核心渲染引擎-->
<dependency>
  <groupId>org.apache.velocity</groupId>
  <artifactId>velocity-engine-core</artifactId>
  <version>2.3</version>
</dependency>

```



### 基础案例

input.vue

```vue
<template>
  <div>${jsonData}</div>
</template>
```

```java
@Test
public void testVelocity() throws IOException {
  //engine->template
  VelocityEngine ve = new VelocityEngine();
  ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, "D:\\video-learn\\vue3-admin-learn-doc\\low-code-plateForm\\code-dir");
  ve.init();
  Template template = ve.getTemplate("input.vue");
  //context
  Context context = GeneratorTempUtils.getVelocityContext();
  context.put("jsonData", "我是tmp模板数据");
  //writer
  FileWriter entityWriter = new FileWriter("D:\\video-learn\\vue3-admin-learn-doc\\low-code-plateForm\\code-dir\\output.vue");
  //生成文件
  template.merge(context, entityWriter);
  entityWriter.close();
}
```



我们提供了线上实时调试 velocity 模板   [线上实时调试](https://github.jzfai.top/low-code-platform/#/template-edit/index)



# 三. 基础语法

## 3.1 VTL介绍

Velocity Template Language (VTL) , 是Velocity 中提供的一种模版语言 , 旨在提供最简单和最干净的方法来将动态内容合并到网页中。简单来说VTL可以将程序中的动态数展示到网页中

VTL的语句分为4大类：**注释** , **非解析内容 ** , **引用**和**指令**。

## 3.2 VTL注释

### 3.2.1 语法

**1. 行注释**

```
## 行注释内容
```

  **2. 块注释**

```
#*
块注释内容1
块注释内容2
*#
```

  **3. 文档注释**

```
#**
文档注释内容1
文档注释内容2
*#
```

###   3.2.1 示例

```velocity
## 我是行注释

#*
* 我是块注释
* 呵呵呵
* *#

#**
* 我是文档注释
*${basicConfig.author}
* *#
```



## 3.3 非解析内容 

所谓非解析内容也就是不会被引擎解析的内容。

### 3.3.1 语法

```
#[[
非解析内容1
非解析内容2 
]]#
```

### 3.3.2 示例

```velocity
#[[
直接输出的内容1
直接输出的内容2
${basicConfig.author}
]]#
```

## 3.4 引用

### **3.4.1 变量引用**

 引用语句就是对引擎上下文对象中的属性进行操作。语法方面分为常规语法(`$属性`)和正规语法(${属性})。

#### 语法

```
$变量名, 若上下文中没有对应的变量，则输出字符串"$变量名"
${变量名},若上下文中没有对应的变量，则输出字符串"${变量名}" 
$!变量名, 若上下文中没有对应的变量，则输出空字符串"" 
$!{变量名}, 若上下文中没有对应的变量，则输出空字符串""
```

#### 示例

```html
<h1>引用变量</h1>
常规语法 : $basicConfig.dataTime
正规语法 : ${basicConfig.dataTime}

## 如果获取的变量不存在, 表达式会原样展示 , 如果不想展示 , 可以使用 $!变量名
## 以下写法的含义代表么如果有变量, 那么获取变量值展示, 没有变量展示""
常规语法 : $!basicConfig.dataTime
正规语法 : $!{basicConfig.dataTime}
```



### 3.4.2 属性引用

#### 语法

```
$变量名.属性, 	若上下文中没有对应的变量，则输出字符串"$变量名.属性"
${变量名.属性}	若上下文中没有对应的变量，则输出字符串"${变量名.属性}"
$!变量名.属性	若上下文中没有对应的变量，则输出字符串""
$!{变量名.属性}	若上下文中没有对应的变量，则输出字符串""
```



#### 示例

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>引用属性</h1>
常规语法 : $basicConfig.dataTime --- $basicConfig.dataTime
正规语法 : ${basicConfig.dataTime} --- ${basicConfig.dataTime}
正规语法 : ${basicConfig.dataTime} --- ${basicConfig.dataTime}
正规语法 : $!{basicConfig.dataTime} --- $!{basicConfig.dataTime}
</body>
</html>
```

### 3.4.3 方法引用

方法引用实际就是指方法调用操作，关注点**返回值**和**参数** , 方法的返回值将输出到最终结果中

#### 语法

```
$变量名.方法([入参1[, 入参2]*]?), 常规写法
${变量名.方法([入参1[, 入参2]*]?)}, 正规写法

$!变量名.方法([入参1[, 入参2]*]?), 常规写法
$!{变量名.方法([入参1[, 入参2]*]?)}, 正规写法
```

#### 示例

```html
<h1>引用属性</h1>
$str.split(" ")
${str.split(" ")}
$time.getTime()
${time.getTime()}
```



## 3.5 指令

指令主要用于定义重用模块、引入外部资源、流程控制。指令以 `#` 作为起始字符。

### 3.5.1 流程控制

####  #set

**作用 :** 在页面中声明定义变量

**语法：** `#set($变量 = 值)`

**示例 :** 

```html
<h1>set指令</h1>
#set($str = "hello world")
#set($int = 1)
#set($arr = [1,2])
#set($boolean = true)
#set($map = {"key1":"value1", "key2":"value2"})

## 在字符串中也可以引用之前定义过的变量
#set($str2 = "$str , how are you !")
#set($str3 = '$str , how are you !')
    
<h1>获取set指令定义的变量</h1>
${str}
${int}
${arr}
${boolean}
${map.key1}--${map.key2}
${str2}
${str3}
```



####  #if/#elseif/#else

**作用 :** 进行逻辑判断

**语法 :** 

```
#if(判断条件)
  .........
#elseif(判断条件)
  .........
#else
  .........
#end 
```

**示例 :** 

```html
<h1>if/elseif/else指令</h1>
#set($language="PHP")

#if($language.equals("JAVA"))
    java开发工程师
#elseif($language.equals("PHP"))
    php开发工程师
#else
    开发工程师
#end
```



####  #foreach

**作用 :** 遍历循环数组或者集合

**格式：**

```
#foreach($item in $items)
    ..........
    [#break]
#end
```

> - $items : 需要遍历的对象或者集合
>   - 如果items的类型为map集合, 那么遍历的是map的value
> - $item : 变量名称, 代表遍历的每一项 
> - #break : 退出循环
> - 内置属性 : 
>   - $foreach.index  : 获取遍历的索引 , 从0开始
>   - $foreach.count : 获取遍历的次数 , 从1开始

**示例 :** 

```html
#macro(forExample $list)
    #foreach($item  in   $list)
        $item
        ## 从0开始 ${foreach.index}
        ## 从1开始 ${foreach.count}
    #end
#end
#macro(mapExample $mapData)
    #foreach($value  in   $mapData)
        $value
    #end

    #foreach($entry  in   $mapData.entrySet())
        ${entry.key} --- ${entry.value}
    #end
    ##[#break] 退出循环
#end
```

调用

```velocity
#parse("utils.vm")
#forExample([1,2,3,4,5])
#mapExample({"name":"熊猫哥","age":"28"})
```



### 3.5.2 引入资源

#### #parse

**作用 :** 引入外部资源 , 引入的资源将被引擎所解析

**语法 :** `#parse(resource)`

> - resource可以为**单引号或双引号的字符串**，也可以为**$变量**，内容为外部资源路径。
> - 注意 : 路径如果为相对路径，则以引擎配置的文件加载器加载路径作为参考系



**示例 :** 

```html
#parse("utils.vm")
```



#### #evaluate

**作用  :** 动态计算 , 动态计算可以让我们在字符串中使用变量

**语法 :** `#evalute("计算语句")`

**示例 :** 

```html
<h1>动态计算</h1>
#set($name = "over")

#evaluate("#if($name=='over') over  #else  not over #end")

#if($name=='over')
    over
#else
    not over
#end
```



### 3.5.3 宏指令

**作用 :** 定义重用模块（可带参数）

**语法 :** 

定义语法

```
#macro(宏名 [$arg]?)
   .....
#end
```

调用语法

```
#宏名([$arg]?)
```

**示例 : **

```velocity
#macro(forExample $list)
    #foreach($item  in   $list)
        $item
        ## 从0开始 ${foreach.index}
        ## 从1开始 ${foreach.count}
    #end
#end
```



## 较为常用的指令

```
设置变量
#set()   

定义方法
#macro(宏名 [$arg]?)
   .....
#end
引入文件
#parse

#if
#elseif

#foreach
#end
```



## vscode 

 安装  ： Apache Velocity 插件 



## idea

原生支持 vm 文件