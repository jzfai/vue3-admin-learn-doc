package ${basicConfig.packageName}.mapper;
import ${basicConfig.packageName}.entity.${dbTableConfig.tableNameCase};
/**
*  ${dbTableConfig.tableDesc}Mapper
*
* @author ${basicConfig.author}
* @since ${basicConfig.dataTime}
*/

public interface ${dbTableConfig.tableNameCase}Mapper extends BaseMapper< ${dbTableConfig.tableNameCase} > {

}
