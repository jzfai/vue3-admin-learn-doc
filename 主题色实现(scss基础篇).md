# 前言

本篇主要介绍scss基础入门和vue3-admin-plus中主题色设置，让大家对scss有一个由浅到深的一个学习过程

[scss官方网站](https://www.sass.hk/docs/)

## LESS 和 SCSS 的区别

1、LESS和SCSS都是css的预处理器，可以拥有变量，运算，继承，嵌套的功能，使用两者可以使代码更加的便于阅读和维护。

2、都可以通过自带的插件，转成相对应的css文件。

3、都可以参数混入，可以传递参数的class，就像函数一样

4、嵌套的规则相同，都是class嵌套class



# 如何实时调试scss

vscode  安装插件easy sass 即可

webstrom或idea    settings->watch->scss 添加监听

## 定义变量

```scss
//demo01.scss
$color: "red";

.use-class {
  .inner-class {
    color: $color;
  }
}

$use-color: "blue";
$mixin-color: "red";
```

>通过$开头去定义变量

## 嵌套

```scss
//demo01.scss
$color:"red";
.use-class{
  .inner-class{
    color:$color
  }
}
```



## @use

```scss
//index.scss
@use "./demo01";
//全部导入
@use "./demo01" as *;
//设置别名aul
@use "./demo01" as aul;
//使用变量：aul.$color
.fai{
  color: aul.$color
}
```

>注：@import 为全导入，@use可以设置别名 和避免 变量冲突或覆盖





## 混合和函数

```scss
//demo02.scss
//混合和函数
$namespace:"el";
@mixin mixClass($block) {
  //注 !global 会把遍历$B提升到根目录
  $B: $namespace + '-' + $block !global;
  .#{$B} {
    @content;
  }
  //@at-root将 root-class提升到全局中
  @at-root {
    .root-class {
      color:red;
    }
  }
}
//函数
@function functionTest($selector) {@return $selector;}
```

使用

```scss
//index.scss
@use "./demo02" as *;
@include mixClass("666"){
  margin-right: 10px;
}
//全局变量演示 $B
.fai{
  color:$B;
  //函数
  font-family: functionTest("fai2222");
}
```



## map和for循环演示 

```scss
//demo04.scss
/*循环和map*/
@use 'sass:map';
$colors: () !default;
$colors: map.deep-merge(
    (
      'white': #ffffff,
      'black': #000000,
      'primary': (
        'base': #409eff
      ),
      'success': (
        'base': #67c23a
      ),
      'warning': (
        'base': #e6a23c
      ),
      'danger': (
        'base': #f56c6c
      ),
      'error': (
        'base': #f56c6c
      ),
      'info': (
        'base': #909399
      )
    ),
    $colors
);
//取map值
$color-info: map.get($colors, 'info', 'base') !default;

//for循环
$types: primary, success, warning, danger, error, info;
```

#### 使用

```scss
//index.scss
/*map和for循环演示*/
@use "./demo03" as *;

//去map值
.map-color{
  color:$color-info;
}

//for循环遍历数组
@each $type in $types {
  @for $i from 1 through 9 {
    .el-#{$type}-#{$i} { //el-primary-1
      color: red;
    }
  }
}
```



## root 和 var

css3中的知识点，element-plus或vue3-admin-plus 实现主题色变更的重要知识点

```scss
//demo05.scss
// css3 var()
:root{
  --el-button-size-large:24px;
}
```

使用

```scss
//index.scss
//读取css3中全局变量的值
.get-root-var{
  font-size:var(--el-button-size-large); //font-size:24px
}
```



此时常用用scss知识点，我们说完了，在element-plus的主题中重点关注 root和var 知识点