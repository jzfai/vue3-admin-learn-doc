## 前言

之前基础篇我们已经讲解了keep-alive基础使用，本篇主要讲解架构中keep-alive的二级和三级路由页面自动缓存，tab缓存原理等

[keep-alive体验地址](https://github.jzfai.top/vue3-admin-template/#/writing-demo/keep-alive)

## 配置

src/store/basic.js

```typescript
import { nextTick } from 'vue'
import { defineStore } from 'pinia'
import defaultSettings from '@/settings'
import router, { constantRoutes } from '@/router'
export const useBasicStore = defineStore('basic', {
  state: () => {
    return {
      token: '',
      getUserInfo: false,
      userInfo: { username: '', avatar: '' }, //user info
      //router
      allRoutes: [],
      buttonCodes: [],
      filterAsyncRoutes: [],
      roles: [],
      codes: [],
      //keep-alive
      cachedViews: [], //用于二级路由页面缓存
      cachedViewsDeep: [],//用于三级路由页面缓存
      //other
      sidebar: { opened: true },
      //axios req collection
      axiosPromiseArr: [],
      settings: defaultSettings
    }
  },
  persist: {
    storage: localStorage,
    paths: ['token']
  },
  actions: {
    setToken(data) {
      this.token = data
    },
    setFilterAsyncRoutes(routes) {
      this.$patch((state) => {
        state.filterAsyncRoutes = routes
        state.allRoutes = constantRoutes.concat(routes)
      })
    },
    setUserInfo({ userInfo, roles, codes }) {
      const { username, avatar } = userInfo
      this.$patch((state) => {
        state.roles = roles
        state.codes = codes
        state.getUserInfo = true
        state.userInfo.username = username
        state.userInfo.avatar = avatar
      })
    },
    resetState() {
      this.$patch((state) => {
        state.token = '' //reset token
        state.roles = []
        state.codes = []
        //reset router
        state.allRoutes = []
        state.buttonCodes = []
        state.filterAsyncRoutes = []
        //reset userInfo
        state.userInfo.username = ''
        state.userInfo.avatar = ''
      })
      this.getUserInfo = false
    },
    resetStateAndToLogin() {
      this.resetState()
      nextTick(() => {
        router.push({ path: '/login' })
      })
    },
    setSidebarOpen(data) {
      this.$patch((state) => {
        state.sidebar.opened = data
      })
    },
    setToggleSideBar() {
      this.$patch((state) => {
        state.sidebar.opened = !state.sidebar.opened
      })
    },

    /*keepAlive缓存*/
    addCachedView(view) {
      this.$patch((state) => {
        if (state.cachedViews.includes(view)) return
        state.cachedViews.push(view)
      })
    },

    delCachedView(view) {
      this.$patch((state) => {
        const index = state.cachedViews.indexOf(view)
        index > -1 && state.cachedViews.splice(index, 1)
      })
    },
    /*third  keepAlive*/
    addCachedViewDeep(view) {
      this.$patch((state) => {
        if (state.cachedViewsDeep.includes(view)) return
        state.cachedViewsDeep.push(view)
      })
    },
    delCacheViewDeep(view) {
      this.$patch((state) => {
        const index = state.cachedViewsDeep.indexOf(view)
        index > -1 && state.cachedViewsDeep.splice(index, 1)
      })
    }
  }
})
```

src/router/index.js

```javascript
  {
    path: '/keep-alive',
    component: Layout,
    name: 'keepAlive',
    meta: { title: 'Tab KeepAlive' },
    children: [
      {
        path: '/second-keep-alive',
        component: () => import('@/views/basic-demo/keep-alive/index.vue'),
        name: 'SecondKeepAlive',
        //cachePage: cachePage when page enter, default false
        //leaveRmCachePage: remove cachePage when page leave, default false
        meta: { title: 'Second KeepAlive', cachePage: true, leaveRmCachePage: false }
      },
      {
        path: '/second-child',
        name: 'SecondChild',
        hidden: true,
        component: () => import('@/views/basic-demo/keep-alive/second-child.vue'),
        meta: { title: 'SecondChild', cachePage: true, activeMenu: '/basic-demo/second-keep-alive' }
      },
      {
        path: 'third-child',
        name: 'ThirdChild',
        hidden: true,
        component: () => import('@/views/basic-demo/keep-alive/third-child.vue'),
        meta: { title: 'ThirdChild', cachePage: true, activeMenu: '/basic-demo/second-keep-alive' }
      },
      //tab-keep-alive
      {
        path: 'tab-keep-alive',
        component: () => import('@/views/basic-demo/keep-alive/tab-keep-alive.vue'),
        name: 'TabKeepAlive',
        //closeTabRmCache: remove cachePage when tabs close, default false
        meta: { title: 'Tab KeepAlive', cachePage: true, closeTabRmCache: true }
      },
      //third-keep-alive
      {
        path: 'third-keep-alive',
        name: 'ThirdKeepAlive',
        component: () => import('@/views/basic-demo/keep-alive/third-keep-alive.vue'),
        //注：移除父容器页面缓存会把子页面一起移除了
        meta: { title: 'Third KeepAlive', cachePage: true, leaveRmCachePage: false },
        alwaysShow: true,
        children: [
          {
            path: 'second-children',
            name: 'SecondChildren',
            component: () => import('@/views/basic-demo/keep-alive/third-children/SecondChildren.vue'),
            meta: { title: 'SecondChild', cachePage: true, leaveRmCachePage: true }
          },
          {
            path: 'third-children',
            name: 'ThirdChildren',
            component: () => import('@/views/basic-demo/keep-alive/third-children/ThirdChildren.vue'),
            meta: { title: 'ThirdChildren', cachePage: true, leaveRmCachePage: false }
          }
        ]
      }
    ]
  }
```



复制页面  src/views/basic-demo/keep-alive/**

复制页面  src/hooks/**

修改 src/layout/app-main/index.vue

```javascript
<script setup>
import { computed, watch } from 'vue'
import { storeToRefs } from 'pinia/dist/pinia'
import { useRoute } from 'vue-router'
import { useBasicStore } from '@/store/basic'
import { cloneDeep } from '@/hooks/use-common'
const { settings, cachedViews } = storeToRefs(useBasicStore())
const route = useRoute()
const key = computed(() => route.path)
/*listen the component name changing, then to keep-alive the page*/
// cachePage: is true, keep-alive this Page
// leaveRmCachePage: is true, keep-alive remote when page leave
let oldRoute = {}
let deepOldRouter = null
const basicStore = useBasicStore()
const removeDeepChildren = (deepOldRouter) => {
  deepOldRouter.children?.forEach((fItem) => {
    basicStore.delCacheViewDeep(fItem.name)
  })
}
watch(
  () => route.name,
  () => {
    const routerLevel = route.matched.length
    //二级路由处理
    if (routerLevel === 2) {
      if (deepOldRouter?.name) {
        if (deepOldRouter.meta?.leaveRmCachePage && deepOldRouter.meta?.cachePage) {
          basicStore.delCachedView(deepOldRouter.name)
          //remove the deepOldRouter‘s children component
          //这样
          removeDeepChildren(deepOldRouter)
        }
      } else {
        if (oldRoute?.name) {
          if (oldRoute.meta?.leaveRmCachePage && oldRoute.meta?.cachePage) {
            basicStore.delCachedView(oldRoute.name)
          }
        }
      }

      if (route.name) {
        if (route.meta?.cachePage) {
          basicStore.addCachedView(route.name)
        }
      }
      deepOldRouter = null
    }

    //三级路由处理
    if (routerLevel === 3) {
      //三级时存储当前路由对象的上一级
      const parentRoute = route.matched[1]
      //deepOldRouter不为空，且deepOldRouter不是当前路由的父对象，则需要清除deepOldRouter缓存
      //一般为三级路由跳转三级路由的情况
      if (deepOldRouter?.name && deepOldRouter.name !== parentRoute.name) {
        if (deepOldRouter.meta?.leaveRmCachePage && deepOldRouter.meta?.cachePage) {
          basicStore.delCachedView(deepOldRouter.name)
          //remove the deepOldRouter‘s children component
          removeDeepChildren(deepOldRouter)
        }
      } else {
        //否则走正常两级路由处理流程
        if (oldRoute?.name) {
          if (oldRoute.meta?.leaveRmCachePage && oldRoute.meta?.cachePage) {
            basicStore.setCacheViewDeep(oldRoute.name)
          }
        }
      }
      //取的是第二级的name
      if (parentRoute.name && parentRoute.meta?.cachePage) {
        deepOldRouter = parentRoute
        basicStore.addCachedView(deepOldRouter.name)
        if (route.name) {
          if (route.meta?.cachePage) {
            //和第三级的name进行缓存
            basicStore.addCachedViewDeep(route.name)
          }
        }
      }
    }
    oldRoute = cloneDeep({ name: route.name, meta: route.meta })
  },
  { immediate: true }
)
</script>
```





## 如何使用

在meta里设置**cachePage**或者**leaveRmCachePage**，决定是否需要缓存和移除缓存
各种组合情况

```javascript
cachePage: true, leaveRmCachePage: true  ->页面进入时缓存，离开时移除缓存
cachePage: false 或者不写  ->页面不缓存，按正常的页面走
cachePage: true, leaveRmCachePage: false  -> 页面进入时缓存，离开时不移除缓存。那么此页面缓存会一直存在，除非手动移除
```

注意: 每个需要缓存的组件需要设置**组件名字**
组件设置的名字要和路由的**name**相同，因为keepAlive缓存就是根据组件的名字缓存的

```javascript
<!--
使用keep-alive
1.设置name（必须）
2.在路由配置处设置cachePage：即可缓存
-->
<script setup name="KeepAlive">

</script>

//路由的name
{
    path: 'keep-alive',
    component: () => import('@/views/example/keep-alive'),
    name: 'KeepAlive',
}
```

## keep-alive核心源码分析

## 二级路由缓存

[多级路由页面缓存体验地址](https://github.jzfai.top/vue3-admin-template/#writing-demo/deep-router-keep-alive/deep-children)

src/layout/components/AppMain.vue

```javascript
<script setup>
import { useBasicStore } from '@/store/basic'
const appStore = useBasicStore()
const route = useRoute()
const settings = computed(() => {
  return appStore.settings
})

const key = computed(() => route.path)
//cachedViews: Array<string>  存储页面name
const cachedViews = computed(() => {
  return appStore.cachedViews
})

let oldRoute = {}
let deepOldRouter = null

//移除当前页下的children缓存
const removeDeepChildren = (deepOldRouter) => {
  deepOldRouter.children?.forEach((fItem) => {
    appStore.setCacheViewDeep(fItem.name)
  })
}

// cachePage: true  ->页面初始化后缓存本页面
// leaveRmCachePage: true -> 页面离开后或者关闭后， 移除本页面缓存 
// leaveRmCachePage和cachePage来自于router里的配置，请看下面介绍

//注：
// appStore.cachedViews:控制二级路由缓存
// appStore.cachedViewsDeep:控制三级路由缓存

//代码原理：通过监听路由里的name。从而获取当前路由，根据路由配置信息里的cachePage和leaveRmCachePage决定是否需要缓存和移除缓存
watch(
  () => route.name,
  () => {
    //获取几级路由,如：routerLevel === 2 二级路由
    const routerLevel = route.matched.length
    //二级路由处理
    if (routerLevel === 2) {
      /**判断路由离开页面时是否需要移除缓存**/
      if (deepOldRouter?.name) {
        //页面离开时，如果有cachePage=true和leaveRmCachePage=true，则移除当前页面缓存    
        if (deepOldRouter.meta?.leaveRmCachePage && deepOldRouter.meta?.cachePage) {
          appStore.delCachedView(deepOldRouter.name)
          //remove the deepOldRouter‘s children component
          removeDeepChildren(deepOldRouter)
        }
      } else {
        if (oldRoute?.name) {
         //页面离开时，如果有cachePage=true和leaveRmCachePage=true，则移除当前页面缓存  
          if (oldRoute.meta?.leaveRmCachePage && oldRoute.meta?.cachePage) {
           //移除缓存
            appStore.delCachedView(oldRoute.name)
          }
        }
      }
      /**判断路由进入页面时是否需要添加缓存**/
      if (route.name) {
        //页面进入时如果有cachePage=true，则设置页面缓存
        if (route.meta?.cachePage) {
          //移除缓存
          appStore.addCachedView(route.name)
        }
      }
      deepOldRouter = null
    }
    //三级路由处理
    if (routerLevel === 3) {
      //三级时存储当前路由对象的上一级
      const parentRoute = route.matched[1]
      
      /**判断路由离开页面时是否需要移除缓存**/
      //deepOldRouter不为空，且deepOldRouter不是当前路由的父对象，则需要清除deepOldRouter缓存
      //一般为三级路由跳转三级路由的情况
      if (deepOldRouter?.name && deepOldRouter.name !== parentRoute.name) {
        if (deepOldRouter.meta?.leaveRmCachePage && deepOldRouter.meta?.cachePage) {
          appStore.delCachedView(deepOldRouter.name)
          //remove the deepOldRouter‘s children component
          removeDeepChildren(deepOldRouter)
        }
      } else {
        //否则走正常两级路由处理流程
        if (oldRoute?.name) {
          if (oldRoute.meta?.leaveRmCachePage && oldRoute.meta?.cachePage) {
            appStore.setCacheViewDeep(oldRoute.name)
          }
        }
      }
      /**判断路由进入页面时是否需要添加缓存**/
      //取的是第二级的name
      if (parentRoute.name && parentRoute.meta?.cachePage) {
        deepOldRouter = parentRoute
        appStore.addCachedViewDeep(deepOldRouter.name)
        if (route.name) {
          if (route.meta?.cachePage) {
            //第三级路由的页面进行缓存，通过route.name
            appStore.addCachedViewDeep(route.name)
          }
        }
      }
    }
    //保存上一个路由信息（也就是当前离开页面的路由信息）
    oldRoute = JSON.parse(JSON.stringify({ name: route.name, meta: route.meta }))
  },
  //首次进入页面监听就触发  
  { immediate: true }
)
</script>
```



## 三级路由页面缓存

```javascript
<script setup>
import { useAppStore } from '@/store/app'
const appStore = useAppStore()
const route = useRoute()
const settings = computed(() => {
  return appStore.settings
})

const key = computed(() => route.path)
//cachedViews: Array<string>  存储页面name
const cachedViews = computed(() => {
  return appStore.cachedViews
})

let oldRoute = {}
let deepOldRouter = null

//移除当前页下的children缓存
const removeDeepChildren = (deepOldRouter) => {
  deepOldRouter.children?.forEach((fItem) => {
    appStore.setCacheViewDeep(fItem.name)
  })
}

// cachePage: true  ->页面初始化后缓存本页面
// leaveRmCachePage: true -> 页面离开后或者关闭后， 移除本页面缓存 
// leaveRmCachePage和cachePage来自于router里的配置，请看下面介绍

//注：
// appStore.cachedViews:控制二级路由缓存
// appStore.cachedViewsDeep:控制三级路由缓存

//代码原理：通过监听路由里的name。从而获取当前路由，根据路由配置信息里的cachePage和leaveRmCachePage决定是否需要缓存和移除缓存
watch(
  () => route.name,
  () => {
    //获取几级路由,如：routerLevel === 2 二级路由
    const routerLevel = route.matched.length
    //三级路由处理
    if (routerLevel === 3) {
      //三级时存储当前路由对象的上一级
      const parentRoute = route.matched[1]
      
      /**判断路由离开页面时是否需要移除缓存**/
      //deepOldRouter不为空，且deepOldRouter不是当前路由的父对象，则需要清除deepOldRouter缓存
      //一般为三级路由跳转三级路由的情况
      if (deepOldRouter?.name && deepOldRouter.name !== parentRoute.name) {
        if (deepOldRouter.meta?.leaveRmCachePage && deepOldRouter.meta?.cachePage) {
          appStore.delCachedView(deepOldRouter.name)
          //remove the deepOldRouter‘s children component
          removeDeepChildren(deepOldRouter)
        }
      } else {
        //否则走正常两级路由处理流程
        if (oldRoute?.name) {
          if (oldRoute.meta?.leaveRmCachePage && oldRoute.meta?.cachePage) {
            appStore.setCacheViewDeep(oldRoute.name)
          }
        }
      }
      /**判断路由进入页面时是否需要添加缓存**/
      //取的是第二级的name
      if (parentRoute.name && parentRoute.meta?.cachePage) {
        deepOldRouter = parentRoute
        appStore.addCachedViewDeep(deepOldRouter.name)
        if (route.name) {
          if (route.meta?.cachePage) {
            //第三级路由的页面进行缓存，通过route.name
            appStore.addCachedViewDeep(route.name)
          }
        }
      }
    }
    //保存上一个路由信息（也就是当前离开页面的路由信息）
    oldRoute = JSON.parse(JSON.stringify({ name: route.name, meta: route.meta }))
  },
  //首次进入页面监听就触发  
  { immediate: true }
)
</script>
```

>目前仅支持2级和3级路由之间的页面缓存，清除缓存
>
>如果清楚缓存的页面中含有children页面缓存，children页面也会一起清除



## 页面组缓存

有时我们会有这种业务场景

![1644546522483](https://github.jzfai.top/file/vap-assets/1644546522483.png)

从A页面跳到B页面在到C页面，此时需要A,B,C页面都需要缓存。保存A,B,C页面的状态，如A页面的列表搜索条件等。但是如果跳出A,B,C页面时需要同时清空A,B,C页面的缓存，如：

![1644546982434](https://github.jzfai.top/file/vap-assets/1644546982434.png)

##### 核心代码

src/views/example/keep-alive/index.vue

```javascript
const $route = useRoute()
const $store = useStore()
// cacheGroup为缓存分组  KeepAlive->routerDemoF->routerDemoS
const cacheGroup = ['SecondKeepAlive', 'SecondChild', 'ThirdChild']
const unWatch = watch(
  () => $route.name,
  () => {
    //如果进入的页面路由name没有在cacheGroup中，则清空这个cacheGroup配置的页面缓存
    if (!cacheGroup.includes($route.name)) {
      //sleep(300) -> 等进入其他页面后在进行页面缓存清空， 用于页面性能优化
      useCommon()
        .sleep(300)
        .then(() => {
          //遍历cacheGroup清空页面缓存
          cacheGroup.forEach((fItem) => delCachedView(fItem))
        })
      //remove watch
      unWatch()
    }
  },
  //deep: true
  //immediate进入页面立即监听
  { immediate: true }
)
```

>SecondKeepAlive', 'SecondChild', 'ThirdChild' 首次进入页面将这一组页面进行缓存，通过监听路由的方式，当离开这个组后移除整个组页面缓存



## tab标签栏缓存

##### 注：缓存和tab没有关联，和路由配置有关联

架构为什么要这样设置呢？

1.缓存和tab没有关联，更利于缓存的灵活配置。如：当我们在settings.js中设置**showTagsView**为false时，依然可以使用路由配置的**cachePage**或者**leaveRmCachePage**进行设置缓存，**TagsView**的显示和隐藏对缓存没有影响。

2.和路由配置有关联，更利于我们对缓存的使用。如，我们可以根据路由配置的**cachePage**或者**leaveRmCachePage**，实现进行页面是否缓存，和离开页面页面是否移除缓存的**组合式选择**。

![1644548683277](https://github.jzfai.top/file/vap-assets/1644548683277.png)



##### 那么如果我想实现之前tab打开时，页面缓存，tab关闭时，移除缓存的功能呢？

在想实现此功能页面的路由上设置

```javascript
//如果配置了cachePage: true 则当前页面进入后，进行缓存。 默认是false
//closeTabRmCache：true 则当前页离开后，页面会被移除缓存。默认是false
meta: { title: 'Tab KeepAlive', cachePage: true, closeTabRmCache: true }
```

>cachePage: true, closeTabRmCache: ture -> 进入时缓存，关闭时移除缓存



