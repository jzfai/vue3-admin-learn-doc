# 前言

vue3-admin 系列， 用户角色权限中心，低代码平台 ， vue3-admin官方文档等。都希望大家能积极贡献，那么本篇介绍下如何进行开源贡献

此处我们 vue3-admin-template 为例



## fork项目到本地

![1669882358474](assets/1669882358474.png)

创建项目-> git clone 项目进行开发



## git clone 项目开发

![1669882568443](assets/1669882568443.png)



开发部分功能并提交

注：提交 commmit 前请先进行git配置（请看下面）



## 配置git的参数

本地和全局配置一个即可

```shell
###本地
git config --local  user.name "jzfai"
git config --local user.email "869653722@qq.com"

###本地
git config --local user.name "xxx"
git config --local user.email "xxxxxx@qq.com"

###全局
git config   user.name "xxx"
git config   user.email "xxxxxxxxx@qq.com"


###本地
git config  user.name "jzfai"
git config  user.email "869653722@qq.com"

```

>注：提交代码前一定要配置你的 **用户名** 和 **邮箱** ，如果配置不正确 ，github不会对你的代码进行 **统计**，因而不会将您加入到项目贡献者中



## 将提交的功能同步到本地库

##### 点击Compare

![1669885024876](assets/1669885024876.png)



##### 发送合并请求

![1669885042226](assets/1669885042226.png)



##### 输入修改内容并提交

![1669885100614](assets/1669885100614.png)



## 将提交的功能提交到主库

##### 推送合并

![1669891021381](assets/1669891021381.png)



##### 创建pull request 合并到主库

![1669891045508](assets/1669891045508.png)

![1669891068407](assets/1669891068407.png)



此时主库中就可以看到 pull  记录了



##### 总的来说：首先同步到本地库，让后在提交到主库





## 修改提交记录脚本

有时我们需要修改提交记录，这里提供修改记录脚本

```sh
#!/bin/sh
git filter-branch -f --env-filter '
OLD_EMAIL="kuhu8866"
CORRECT_NAME="jzfai"
CORRECT_EMAIL="869653722@qq.com"
if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
```



