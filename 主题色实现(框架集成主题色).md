# 前言

本篇主要介绍vue3-admin系列主题色集成过程

[scss中文官网](https://www.sass.hk/docs/)

## 基础色

新建基础色文件
src/theme/base/element-plus/var.scss

```scss
/* Element Chalk Variables */
@use 'sass:math';
@use 'sass:map';
@use '../../mixins/function.scss' as *;
// types
$types: primary, success, warning, danger, error, info;
// change color
$colors: () !default;
$colors: map.deep-merge(
  (
    'white': #ffffff,
    'black': #000000,
    'primary': (
      'base': #c72210//#409eff
    ),
    'success': (
      'base': #45b207
    ),
    'warning': (
      'base': #ec8828
    ),
    'danger': (
      'base': #f56c6c
    ),
    'error': (
      'base': #d24934
    ),
    'info': (
      'base': #909399
    )
  ),
  $colors
);
$color-white: map.get($colors, 'white') !default;
$color-black: map.get($colors, 'black') !default;
$color-primary: map.get($colors, 'primary', 'base') !default;
$color-success: map.get($colors, 'success', 'base') !default;
$color-warning: map.get($colors, 'warning', 'base') !default;
$color-danger: map.get($colors, 'danger', 'base') !default;
$color-error: map.get($colors, 'error', 'base') !default;
$color-info: map.get($colors, 'info', 'base') !default;
//$colors添加 --el-color-primary-light-7
@mixin set-color-mix-level($type, $number, $mode: 'light', $mix-color: $color-white) {
  $colors: map.deep-merge(
    (
      $type: (
        '#{$mode}-#{$number}': mix($mix-color, map.get($colors, $type, 'base'), math.percentage(math.div($number, 10)))
      )
    ),
    $colors
  ) !global;
}
// $colors.primary.light-i
@each $type in $types {
  @for $i from 1 through 9 {
    @include set-color-mix-level($type, $i, 'light', $color-white);
  }
}
```

src/theme/base/element-plus/css-vars.scss

```scss
@use 'sass:map';
@use './var' as *;
@use '../../mixins/var'  as *;
@use '../../mixins/mixins'  as *;
html.china-red {
  color-scheme: china-red;
  @each $type in (primary, success, warning, danger, error, info) {
    @include set-css-color-rgb($colors, $type);
  }

  @each $type in (primary, success, warning, danger, error, info) {
    @include set-css-color-type($colors, $type);
  }
  //--el-color-primary: #c72210;
}
```

var.scss :  源 element-plus变量定义文件
css-vars.scss  根据var.scss 定义的变量生成 “--el-color-primary: #c72210”  类似的变量

##### 注：需要提前复制 src/theme/mixins/**  下的文件 到指定目录下



那么此时我们可以通过动态修改 $colors 里颜色，修改相应基础色



## 组件色

这里以el-button为例

当我们运行页面时，查看了el-button使用到的变量

![1670484619673](assets/1670484619673.png)

下面我们根据这些变量进行修改

src/theme/base/element-plus/button.scss

```scss
html.base-theme {
  .at-button-low {
    --el-button-text-color: #262626;
    --el-button-bg-color: #c72210;
    --el-button-border-color: #d9d9d9;
    --el-button-outline-color: #d9d9d9;

    --el-button-hover-text-color: #c72210;
    --el-button-hover-link-text-color: #c72210;
    --el-button-hover-bg-color: #ffece6;
    --el-button-hover-border-color: transparent;

    --el-button-active-color: #a8150a;
    --el-button-active-bg-color: #a8150a;
    --el-button-active-border-color: transparent;

    --el-button-disabled-text-color: #a6a6a6;
    --el-button-disabled-bg-color: #ffece6;
    --el-button-disabled-border-color: #c72210;
    //loading
    --el-button-loading-text-color: #c72210;
    --el-button-loading-bg-color: #ffece6;
    --el-button-loading-border-color: #c72210;
  }
}
```



## 自定义主题变量

src/theme/base/custom/ct-css-vars.scss

```scss
html.base-theme {
  /*element-plus section */
  --el-custom-color: red;
}
```



此时我们 整合我们的主题色

src/theme/base/index.scss

```scss
//element-plus
@use "./element-plus/css-vars";
@use "./element-plus/var";
@use "./element-plus/button";
//custom
@use "./custom/ct-css-vars";
```

src/theme/index.scss

```scss
//base-theme
@use "./base";
```

main.js中引入

```typescript
//import theme
import './theme/index.scss'
```



## 如何使用

src/views/dashboard/index.vue

```vue
<template>
  <el-button class="at-button-low" type="primary" @click="loginOut">自定义组件测试</el-button>
  <div style="color: var(--el-custom-color)">自定义变量测试</div>
</template>
```

index.html

```html
<html lang="en" class="base-theme">
  //....
</html>
```

>class="base-theme" 使用主题色前缀



总结： 主题色主要分为两部分 ，1. element-plus主题色，2. 自定义主题色

element-plus主题色： 基础色和组件色

自定义主题色：根据自己定义的变量实现颜色变换，如 导航栏蓝色等