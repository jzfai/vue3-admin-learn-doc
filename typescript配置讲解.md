## 前言

上篇我们已经讲了typescript基础，本篇我们继续介绍typescript的配置

[typescript官方配置参考](https://www.typescriptlang.org/zh/tsconfig)

#### 安装依赖

```shell
pnpm add typescript@4.7.2  -D
```

>已经安装，则不需要安装

#### 配置

新建ts配置文件  tsconfig.json

```javascript
{
    //设置files为空,则不会自动扫描默认目录，也就是只会扫描include配置的目录
    "files": [],
    "compilerOptions": {
        //路径设置:一些将模块导入重新映射到相对于 baseUrl 路径的配置。 paths 可以允许你声明 TypeScript 应该如何解析你的 require/import。
        "paths": {
            "@/*": ["src/*"],
            "~/*": ["typings/*"]
        },
        //在这个项目中被配置为 "baseUrl": "."，TypeScript 将会从首先寻找与 tsconfig.json 处于相同目录的文件。
        "baseUrl": ".",
        //编译目标
        "target":"esnext",

        //高阶库
        //ES2019 中额外提供的 API —— array.flat，array.flatMap，Object.fromEntries，string.trimStart，string.trimEnd 等,
        //DOM  定义 —— window，document 等。
        "lib": ["ES2019", "DOM"],

        //ESNext
        //import { valueOfPi } from "./constants";
        //export const twoPi = valueOfPi * 2;
        "module": "esnext",

        //严格的strict
        //该strict标志启用了广泛的类型检查行为，从而增强了程序正确性的保证,您可以根据需要关闭单个严格模式系列检查
        "strict": true,
        //严格的空检查 - strictNullChecks
        "strictNullChecks": false,
        //允许 JS -allowJs 可以运行你在ts文件中引入js文件
        "allowJs": true,
        //检查 JS -checkJs
        //与 allowJs 配合使用，当 checkJs 被启用时，JavaScript 文件中会报告错误。也就是相当于在项目中所有 JavaScript 文件顶部包含 // @ts-check。
        "checkJs": false,

        //jsx控制 JSX 在 JavaScript 文件中的输出方式。 这只影响 .tsx 文件的 JS 文件输出。
        //保留: "preserve"：jsx中写的语法原样输出
        "jsx": "preserve",


        //为你工程中的每个 TypeScript 或 JavaScript 文件生成 .d.ts 文件。
        "declaration": false,

        //移除注释 -removeComments
        //当转换为 JavaScript 时，忽略所有 TypeScript 文件中的注释。默认为 false
        "removeComments": true,

        //允许声明any类型
        "noImplicitAny": true,
        //在“any”类型的“this”表达式上引发错误。
        "noImplicitThis": true,

        //保留常量枚举 -preserveConstEnums
        //不要删除const enum生成代码中的声明。const enums 提供了一种通过发出枚举值而不是引用来减少应用程序在运行时的整体内存占用的方法
        "preserveConstEnums": true,
        //和noUnusedLocals一样，针对func
        "noUnusedParameters": false,

        //是否输出src2.js.map文件
        "sourceMap": false,

        //tsc编译后输出的目录
        "outDir": "./ts-out-dir",

        //没有未使用的变量 noUnusedLocals
        //报告未使用的局部变量的错误。
        "noUnusedLocals": false,

        //解析 JSON 模块 -resolveJsonModule
        //是否允许把json文件当做模块进行解析
        //TypeScript 默认不支持解析 JSON 文件
        "resolveJsonModule": true,

        //模块解析 -moduleResolution
        //指定模块解析策略：'node' （Node.js） 或 'classic' （在 TypeScript 1.6 版本之前使用）。 你可能不需要在新代码中使用 classic。
        "moduleResolution": "Node",

        //ES 模块互操作性 -esModuleInterop
        //默认情况下（未设置 `esModuleInterop` 或值为 false），TypeScript 像 ES6 模块一样对待
        "esModuleInterop": true,

        //忽略所有的声明文件（ *.d.ts）的类型检查。
        "skipLibCheck": true
    },
    //指定要包含在程序中的文件名或模式数组。这些文件名是相对于包含该tsconfig.json文件的目录进行解析的。
    "include": ["src", "typings"],
    //它不是一种阻止文件包含在代码库中的机制 - 它只是更改include设置找到的内容
    "exclude": ["node_modules", "**/dist"]
}

```

### paths

一些将模块导入重新映射到相对于 baseUrl 路径的配置。 paths 可以允许你声明 TypeScript 应该如何解析你的 require/import。

```vue
<script setup lang="ts">
import App from "@/App.vue"
</script>
```

>配置path后ts能识别@符号



### allowJs 和 checkJs

配置后ts和js可以互相引入，实现了js和ts混合开发



### moduleResolution和esModuleInterop

配置后可以在ts中使用 es6的模块化语法，并且

```javascript
import path from "path" 
```

>path不会报错





以上可以建议参考[typescript官方配置](https://www.typescriptlang.org/zh/tsconfig)一起学习



源码和视频地址

[typescript基础](https://gitee.com/jzfai/vue3-admin-learn-code/tree/typescript%E5%9F%BA%E7%A1%80/)

