# 前言

 Vite 插件扩展了设计出色的 Rollup 接口，带有一些 Vite 独有的配置项。因此，你只需要编写一个 Vite 插件，就可以同时为开发环境和生产环境工作。 

[vite插件官方文档](https://cn.vitejs.dev/guide/api-plugin.html)



## 约定


 Vite 插件应该有一个带 `vite-plugin-` 前缀、语义清晰的名称 

- `vite-plugin-vue-` 前缀作为 Vue 插件
- `vite-plugin-react-` 前缀作为 React 插件
- `vite-plugin-svelte-` 前缀作为 Svelte 插件



## 通用钩子

在开发中，Vite 开发服务器会创建一个插件容器来调用 [Rollup 构建钩子](https://rollupjs.org/guide/en/#build-hooks)，与 Rollup 如出一辙。

以下钩子在服务器启动时被调用：

- [`options`](https://rollupjs.org/guide/en/#options)
- [`buildStart`](https://rollupjs.org/guide/en/#buildstart)

以下钩子会在每个传入模块请求时被调用：

- [`resolveId`](https://rollupjs.org/guide/en/#resolveid)
- [`load`](https://rollupjs.org/guide/en/#load)
- [`transform`](https://rollupjs.org/guide/en/#transform)

以下钩子在服务器关闭时被调用：

- [`buildEnd`](https://rollupjs.org/guide/en/#buildend)
- [`closeBundle`](https://rollupjs.org/guide/en/#closebundle)



>注：resolveId , transform 为重点钩子 



#### reslveId   

模块id确认

```javascript
// id确认
resolveId ( source ) {   //index.html
  if (source === 'index.html') {
    console.log('resolvedId', source);
    return source; // 返回source表明命中，vite不再询问其他插件处理该id请求
  }
  return null; // 返回null表明是其他id要继续处理
}
```

>有助于提高插件效率

### transform ***

匹配文件，转换文件内容，并返回转换后的内容

```javascript
// 转换
transform(code, id) {
  if (id === 'index.html') { //index.html  title
    //匹配到需要转换的id
    console.log('transform');
  }
  return code
}
```

>核心钩子，一般用于匹配文件，修改文件内容并返回



## Vite 独有钩子

 Vite 插件也可以提供钩子来服务于特定的 Vite 目标。这些钩子会被 Rollup 忽略。 

## `config`

 在解析 Vite 配置前调用。钩子接收原始用户配置（命令行选项指定的会与配置文件合并）和一个描述配置环境的变量，包含正在使用的 `mode` 和 `command`。它可以返回一个将被深度合并到现有配置中的部分配置对象，或者**直接改变配置**（如果默认的合并不能达到预期的结果） 

```typescript
// 返回部分配置（推荐）
const partialConfigPlugin = () => ({
  name: 'return-partial',
  config: () => ({
    resolve: {
      alias: {
        foo: 'bar'
      }
    }
  })
})

// 直接改变配置（应仅在合并不起作用时使用）
const mutateConfigPlugin = ({title:"熊猫哥"}) => ({
  name: 'mutate-config',
  config(config, { command }) {
    if (command === 'build') {
      config.root = 'foo'
       
    }
  }
})
```



### `configResolved`

 在解析 Vite 配置后调用。使用**这个钩子读取和存储最终解析的配置**。当插件需要根据运行的命令做一些不同的事情时，它也很有用。 

```typescript
const examplePlugin = () => {
  let config

  return {
    name: 'read-config',

    configResolved(resolvedConfig) {
      // 存储最终解析的配置
      config = resolvedConfig
    },

    // 在其他钩子中使用存储的配置
    transform(code, id) {
      if (config.command === 'serve') {
        // dev: 由开发服务器调用的插件
      } else {
        // build: 由 Rollup 调用的插件
      }
    }
  }
}
```

### `transformIndexHtml`

** 转换 `index.html` 的专用钩子**。钩子接收当前的 HTML 字符串和转换上下文。上下文在开发期间暴露[`ViteDevServer`](https://cn.vitejs.dev/guide/api-javascript.html#vitedevserver)实例，在构建期间暴露 Rollup 输出的包 

```javascript
const htmlPlugin = () => {
  return {
    name: 'html-transform',
    transformIndexHtml(html) {
      return html.replace(
        /<title>(.*?)<\/title>/,
        `<title>Title replaced!</title>`
      )
    }
  }
}
```



## 钩子调用顺序

![1670319953750](assets/1670319953750.png)


注：绿色为vite特有钩子， 黄色为从rollup继承的钩子



## 插件顺序

一个 Vite 插件可以额外指定一个 `enforce` 属性（类似于 webpack 加载器）来调整它的应用顺序。`enforce` 的值可以是`pre` 或 `post`。解析后的插件将按照以下顺序排列：

- Alias
- 带有 `enforce: 'pre'` 的用户插件  --> 拿到我们原始文件代码
- Vite 核心插件
- 没有 enforce 值的用户插件
- Vite 构建用的插件
- 带有 `enforce: 'post'` 的用户插件 
- Vite 后置构建插件（最小化，manifest，报告）



## 如何使用

src/plugins/vite-plugin-my-example.js

```typescript
export default function myExample () {
    // 返回的是插件对象
    return {
      name: 'my-example', // 名称用于警告和错误展示
      // enforce: 'pre'|'post'
      // 初始化hooks，只走一次
      options(opts) {
        console.log('options', opts);
      },
      buildStart() {
        console.log('buildStart');
      },
      config(config) {
        console.log('config', config);
        return {}
      },
      configResolved(resolvedCofnig) {
        console.log('configResolved');
      },
      configureServer(server) {
        console.log('configureServer');
        // server.app.use((req, res, next) => {
        //   // custom handle request...
        // })
      },
      transformIndexHtml(html) {
        console.log('transformIndexHtml');
        return html
        // return html.replace(
        //   /<title>(.*?)<\/title>/,
        //   `<title>Title replaced!</title>`
        // )
      },
      // id确认
      resolveId ( source ) {
        if (source === 'virtual-module') {
          console.log('resolvedId', source);
          return source; // 返回source表明命中，vite不再询问其他插件处理该id请求
        }
        return null; // 返回null表明是其他id要继续处理
      },
      // 加载模块代码
      load ( id ) {
        if (id === 'virtual-module') {
          console.log('load');
          return 'export default "This is virtual!"'; // 返回"virtual-module"模块源码
        }
        return null; // 其他id继续处理
      },
      // 转换
      transform(code, id) {
        if (id === 'virtual-module') {
          console.log('transform');
        }
        return code
      },
    };
  }
```

 vite.config.ts 

```typescript
//vite.config.ts
import myExample from './plugins/vite-plugin-my-example'  //自定义插件
export default defineConfig({
  plugins: [vue(),myExample()] //插件
})
```

### 

